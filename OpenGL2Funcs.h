/*===========================================================================*\
|*  libunr-OpenGL2Drv - An OpenGL2 Render Device for libunr                  *|
|*  Copyright (C) 2018-2019  Adam W.E. Smith                                 *|
|*                                                                           *|
|*  This program is free software: you can redistribute it and/or modify     *|
|*  it under the terms of the GNU General Public License as published by     *|
|*  the Free Software Foundation, either version 3 of the License, or        *|
|*  (at your option) any later version.                                      *|
|*                                                                           *|
|*  This program is distributed in the hope that it will be useful,          *|
|*  but WITHOUT ANY WARRANTY; without even the implied warranty of           *|
|*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the            *|
|*  GNU General Public License for more details.                             *|
|*                                                                           *|
|*  You should have received a copy of the GNU General Public License        *|
|*  along with this program. If not, see <https://www.gnu.org/licenses/>.    *|
\*===========================================================================*/

/*========================================================================
 * OpenGL2Funcs.h - OpenGL extensions used by the renderer
 *
 * written by Adam 'Xaleros' Smith
 *========================================================================
*/

#if defined LIBUNR_WIN32
  #include <Windows.h>
#endif

#include "GL/gl.h"
#include "glext.h"

#if defined LIBUNR_WIN32
  #include "wgl.h"
  #include "wglext.h"

  extern PFNWGLCHOOSEPIXELFORMATARBPROC wglChoosePixelFormatARB;
  extern PFNWGLCREATECONTEXTATTRIBSARBPROC wglCreateContextAttribsARB; 
  #define LOAD_EXT_PROC( proc, type ) \
    proc = (type)wglGetProcAddress(TXT(proc)); \
    if ( proc == NULL ) { \
      GLogf( LOG_ERR, "Failed to get OpenGL extension '%s'", TXT(proc) ); \
      return false; \
    }
#endif

// Buffer extensions
extern PFNGLGENBUFFERSPROC glGenBuffers;
extern PFNGLBINDBUFFERPROC glBindBuffer;
extern PFNGLBUFFERDATAPROC glBufferData;
extern PFNGLDELETEBUFFERSPROC glDeleteBuffers;

// Texture extensions
extern PFNGLACTIVETEXTUREPROC glActiveTexture;

// Shader extensions
extern PFNGLCREATESHADERPROC      glCreateShader;
extern PFNGLSHADERSOURCEPROC      glShaderSource;
extern PFNGLCOMPILESHADERPROC     glCompileShader;
extern PFNGLCREATEPROGRAMPROC     glCreateProgram;
extern PFNGLATTACHSHADERPROC      glAttachShader;
extern PFNGLDELETESHADERPROC      glDeleteShader;
extern PFNGLGETSHADERIVPROC       glGetShaderiv;
extern PFNGLGETSHADERINFOLOGPROC  glGetShaderInfoLog;
extern PFNGLLINKPROGRAMPROC       glLinkProgram;
extern PFNGLUSEPROGRAMPROC        glUseProgram;
extern PFNGLDELETEPROGRAMPROC     glDeleteProgram;
extern PFNGLGETPROGRAMIVPROC      glGetProgramiv;
extern PFNGLGETPROGRAMINFOLOGPROC glGetProgramInfoLog;

// Vertex attribute extensions
extern PFNGLVERTEXATTRIBPOINTERPROC     glVertexAttribPointer;
extern PFNGLENABLEVERTEXATTRIBARRAYPROC glEnableVertexAttribArray;
extern PFNGLDISABLEVERTEXATTRIBARRAYPROC glDisableVertexAttribArray;

// Shader uniform extensions
extern PFNGLUNIFORM1IPROC glUniform1i;
extern PFNGLUNIFORMMATRIX4FVPROC glUniformMatrix4fv;
extern PFNGLGETUNIFORMLOCATIONPROC glGetUniformLocation;
