/*===========================================================================*\
|*  libunr-OpenGL2Drv - An OpenGL2 Render Device for libunr                  *|
|*  Copyright (C) 2018-2019  Adam W.E. Smith                                 *|
|*                                                                           *|
|*  This program is free software: you can redistribute it and/or modify     *|
|*  it under the terms of the GNU General Public License as published by     *|
|*  the Free Software Foundation, either version 3 of the License, or        *|
|*  (at your option) any later version.                                      *|
|*                                                                           *|
|*  This program is distributed in the hope that it will be useful,          *|
|*  but WITHOUT ANY WARRANTY; without even the implied warranty of           *|
|*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the            *|
|*  GNU General Public License for more details.                             *|
|*                                                                           *|
|*  You should have received a copy of the GNU General Public License        *|
|*  along with this program. If not, see <https://www.gnu.org/licenses/>.    *|
\*===========================================================================*/

/*========================================================================
 * OpenGL2RenderDevice.h - OpenGL 2.x 3D rendering device
 *
 * written by Adam 'Xaleros' Smith
 *========================================================================
*/

#pragma once

#include <Util/TQueue.h>
#include <Core/UMath.h>
#include <Engine/UMesh.h>
#include <Engine/URender.h>
#include <Engine/UTexture.h>

#include "OpenGL2Funcs.h"
#include "OpenGL2Shader.h"

/*-----------------------------------------------------------------------------
 * FGLViewport
 * A viewport tied together with a draw context and OpenGL context
-----------------------------------------------------------------------------*/
struct FGLViewport
{
  UViewport* Viewport;
  FMatrix4x4 OrthoMatrix;
  FMatrix4x4 Perspective;

#if defined LIBUNR_WIN32
  HDC DC;
  HGLRC RC;
#endif

  FGLViewport& operator=( FGLViewport& A ) 
  {
    memcpy( &A, this, sizeof( FGLViewport ) );
    return A;
  }
};

/*-----------------------------------------------------------------------------
 * FGLVertex
 * A vertex stored with UV coordinates
-----------------------------------------------------------------------------*/
struct FGLVertex
{
  FVector Vertex;
  float U;
  float V;
};

/*-----------------------------------------------------------------------------
 * FCanvasElement
 * A queued element to be drawn on the canvas after DrawWorld
-----------------------------------------------------------------------------*/
struct FCanvasElement
{
  TArray<FGLVertex> Vertices; // The vertex coordinates to be drawn
  UTexture* Texture;          // The texture that will be drawn with this element
  FGLShaderProgram Shader;    // The shader to be used with this element
};

/*-----------------------------------------------------------------------------
 * UOpenGL2RenderDevice
 * The main OpenGL 2.x rendering subsystem
-----------------------------------------------------------------------------*/
class DLL_EXPORT UOpenGL2RenderDevice : public URenderDevice
{
  DECLARE_NATIVE_CLASS( UOpenGL2RenderDevice, URenderDevice, CLASS_NoExport, OpenGL2Drv )
  UOpenGL2RenderDevice();

  // USubsystem functions
  virtual bool Init();
  virtual bool Exit();
  virtual void Tick( float DeltaTime );

  // URenderDevice functions
  virtual void DrawWorld( ULevel* Level, UViewport* Viewport );
  virtual void DrawActor( AActor* Actor );
  virtual void DrawTile( UTexture* Tex, FBoxInt2D& Dim, FRotator& Rot, float U, float V, float UL, float VL, int PolyFlags = 0 );
  virtual void DrawMesh( UMesh* Mesh, FName AnimSeq, float AnimRate, FVector& Loc, FVector& Scale, FRotator& Rot, int PolyFlags = 0 );
  virtual void DrawGrid( FBox& Dim, FColor& Color );
  virtual bool InitViewport( UViewport* Viewport );
  virtual bool SetActiveViewport( UViewport* Viewport );

  // OpenGL specific functions
  bool GetExtensions();
  bool RegisterTexture( UTexture* Texture );
  bool UnregisterTexture( UTexture* Texture );
  bool CompileShaderPrograms();

  // Per-device render elements
  FGLViewport* CurrentViewport;
  TArray<FGLViewport> Viewports;      // All viewports with rendering contexts
  TArray<FGLShaderProgram> Shaders;   // All registered shader types

  // Per-tick render elements
  TQueue<FCanvasElement> CanvasDrawList; // A list of Canvas->Draw*() elements

#if defined LIBUNR_WIN32
  PIXELFORMATDESCRIPTOR Pfd;
  HGLRC MainRC;
  HGLRC FakeRC;
  HDC FakeDC;
  HWND FakeWnd;
  WNDCLASSEX FakeCls;
#endif
};
