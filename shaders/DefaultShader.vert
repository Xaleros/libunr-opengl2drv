#version 120

uniform mat4 Projection;

attribute vec3 Position;
varying vec2 TexCoord;

void main()
{
	// TODO: Multi texturing!
	gl_TexCoord[0] = gl_MultiTexCoord0;
	gl_Position = Projection * vec4( Position.x, Position.y, Position.z, 1.0 );
}