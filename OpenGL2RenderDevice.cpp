/*===========================================================================*\
|*  libunr-OpenGL2Drv - An OpenGL2 Render Device for libunr                  *|
|*  Copyright (C) 2018-2019  Adam W.E. Smith                                 *|
|*                                                                           *|
|*  This program is free software: you can redistribute it and/or modify     *|
|*  it under the terms of the GNU General Public License as published by     *|
|*  the Free Software Foundation, either version 3 of the License, or        *|
|*  (at your option) any later version.                                      *|
|*                                                                           *|
|*  This program is distributed in the hope that it will be useful,          *|
|*  but WITHOUT ANY WARRANTY; without even the implied warranty of           *|
|*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the            *|
|*  GNU General Public License for more details.                             *|
|*                                                                           *|
|*  You should have received a copy of the GNU General Public License        *|
|*  along with this program. If not, see <https://www.gnu.org/licenses/>.    *|
\*===========================================================================*/

/*========================================================================
 * OpenGL2RenderDevice.cpp - OpenGL 2.x 3D rendering device
 *
 * written by Adam 'Xaleros' Smith
 *========================================================================
*/

#pragma once

#include <Core/UClass.h>
#include <Engine/UEngine.h>
#include <Engine/UWindowsViewport.h>

#include "OpenGL2RenderDevice.h"
#include "OpenGL2Funcs.h"

#if defined LIBUNR_WIN32
  #include <direct.h>
#endif

/*-----------------------------------------------------------------------------
 * Render Device Constructor
-----------------------------------------------------------------------------*/
UOpenGL2RenderDevice::UOpenGL2RenderDevice()
  : URenderDevice()
{
}

/*-----------------------------------------------------------------------------
 * Render Device Destructor
-----------------------------------------------------------------------------*/
UOpenGL2RenderDevice::~UOpenGL2RenderDevice()
{
}

/*-----------------------------------------------------------------------------
 * Render Device Initialization
 * 
 * We do different things here based on what operating systems we can run on
 *
 * For Windows, we do the following
 * - Create a "fake" window for our "fake" OpenGL context
 * - Set up a "fake" OpenGL context so that we can get extension functions
 *
 * After platform specific init, we do the following
 * - Get any extension functions that we require
 * - Set global GL state that won't change after context creation
-----------------------------------------------------------------------------*/
bool UOpenGL2RenderDevice::Init()
{
#if defined LIBUNR_WIN32

  // Setup a fake window to gain access to extensions
  HINSTANCE hInst = GetModuleHandle( NULL );
  memset( &FakeCls, 0, sizeof( FakeCls ) );

  FakeCls.cbSize = sizeof( FakeCls );
  FakeCls.style = CS_HREDRAW | CS_VREDRAW | CS_OWNDC;
  FakeCls.lpfnWndProc = DefWindowProc; // We are not actually doing anything with the window here
  FakeCls.hInstance = hInst;
  FakeCls.hCursor = LoadCursor( NULL, IDC_ARROW );
  FakeCls.lpszClassName = "FakeGL2WndClass";

  if ( !RegisterClassEx( &FakeCls ) )
  {
    GLogf( LOG_CRIT, "Failed to create fake window class for GL 2.x context" );
    return false;
  }

  FakeWnd = CreateWindow( "FakeGL2WndClass", "FakeGL2Window", WS_CLIPSIBLINGS | WS_CLIPCHILDREN, 0, 0, 1, 1, NULL, NULL, hInst, NULL );
  if ( !FakeWnd )
  {
    GLogf( LOG_CRIT, "Failed to create fake window for GL 2.x context" );
    return false;
  }

  // Create fake GL context
  FakeDC = GetDC( FakeWnd );
  memset( &Pfd, 0, sizeof( Pfd ) );

  Pfd.nSize = sizeof( Pfd );
  Pfd.nVersion = 1;
  Pfd.dwFlags = PFD_DRAW_TO_WINDOW | PFD_SUPPORT_OPENGL | PFD_DOUBLEBUFFER;
  Pfd.iPixelType = PFD_TYPE_RGBA;
  Pfd.cColorBits = 32;
  Pfd.cAlphaBits = 8;
  Pfd.cDepthBits = 24;

  int PfdId = ChoosePixelFormat( FakeDC, &Pfd );
  if ( PfdId == 0 )
  {
    GLogf( LOG_CRIT, "Failed to get fake pixel format for GL 2.x context" );
    return false;
  }

  if ( !SetPixelFormat( FakeDC, PfdId, &Pfd ) )
  {
    GLogf( LOG_CRIT, "Failed to set fake pixel format for GL 2.x context" );
    return false;
  }

  FakeRC = wglCreateContext( FakeDC );
  if ( FakeRC == 0 )
  {
    GLogf( LOG_CRIT, "Failed to create fake GL context" );
    return false;
  }

  if ( !wglMakeCurrent( FakeDC, FakeRC ) )
  {
    GLogf( LOG_CRIT, "Failed to make fake GL context the current context" );
    return false;
  }

#else
  GLogf( LOG_ERR, "OpenGL2RenderDevice needs platform specific implementation" );
  return false;
#endif

  // Register needed OpenGL extensions
  if ( !GetExtensions() )
  {
    GLogf( LOG_CRIT, "Failed to get critical extensions" );
    return false;
  }

  glClearColor( 0.0f, 0.0f, 0.0f, 0.0f );
  glEnable( GL_DEPTH_TEST );
  glEnable( GL_BLEND );
  glBlendFunc( GL_SRC_ALPHA, GL_SRC_COLOR );

  return true;
}

/*-----------------------------------------------------------------------------
 * Render Device Exit
 *
 * Here, we free all GL contexts that have been created
 * This is entirely platform dependent
-----------------------------------------------------------------------------*/

bool UOpenGL2RenderDevice::Exit()
{
#if defined LIBUNR_WIN32
  wglMakeCurrent( NULL, NULL );

  // TODO: Free all contexts

  // Destroy fake context
  wglDeleteContext( FakeRC );
  ReleaseDC( FakeWnd, FakeDC );
  DestroyWindow( FakeWnd );
  return true;
#else
  return false;
#endif
}

/*-----------------------------------------------------------------------------
 * Render Device Tick
 *
 * The general order of operations is as follows.
 * - Clear out the currently active framebuffer
 * - Loop through each viewport to perform each following step
 * - Draw the world from the perspective of the camera attached to a viewport
 * - Generate vertex buffers based on the number of elements we have
 * - Set the active texture slot to GL_TEXTURE0
 * - Draw each canvas element
 * - Swap framebuffers so that we can see the results of our frame
 *
 * World drawing performs the following steps
 * - TODO:
 *
 * Drawing canvas elements performs the following steps
 * - Register the texture for this element if needed
 * - Switch to the element's requested shader
 * - Enforce an orthographic projection for this shader
 * - Bind the element's texture and set it's uniform for the frag. shader
 * - Buffer all the vertices for this element
 * - Set up vertex attributes
 * - Draw element
 * - Pop the element from the queue
 * 
 * Current limitations / room for optimization
 * - No clean way to handle detail textures on canvas elements
 * - glGetUniformLocation should be called once for each uniform of interest
-----------------------------------------------------------------------------*/
void UOpenGL2RenderDevice::Tick( float DeltaTime )
{
  // Clear out current framebuffer
  glClear( GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT );

  // TODO: For each viewport...

  // TODO: Draw world

  // Draw any queued canvas element
  size_t NumElements = CanvasDrawList.Size();
  GLuint* VertexBuffers = new GLuint[NumElements];
  glGenBuffers( NumElements, VertexBuffers );

  // We're only using texture0 from here on
  glActiveTexture( GL_TEXTURE0 );

  for ( int i = 0; i < NumElements; i++ )
  {
    FCanvasElement& Element = CanvasDrawList.Front();

    if ( !Element.Texture->TextureHandle )
      RegisterTexture( Element.Texture );

    // Switch to this element's desired shader
    glUseProgram( Element.Shader.Handle );

    // Make sure orthographic matrix is set
    GLint Projection = glGetUniformLocation( Element.Shader.Handle, "Projection" );
    glUniformMatrix4fv( Projection, 1, false, (const GLfloat*)CurrentViewport->OrthoMatrix.Data );

    // Set current texture
    glBindTexture( GL_TEXTURE_2D, (GLuint)Element.Texture->TextureHandle );
    glUniform1i( glGetUniformLocation( Element.Shader.Handle, "Tex0" ), 0 );

    // Push vertices into a buffer
    glBindBuffer( GL_ARRAY_BUFFER, VertexBuffers[i] );
    glBufferData( GL_ARRAY_BUFFER, Element.Vertices.Size() * sizeof( FGLVertex ), Element.Vertices.Data(), GL_STATIC_DRAW );
    glVertexAttribPointer( 0, 3, GL_FLOAT, GL_FALSE, 5 * sizeof( GLfloat ), 0 );
    glVertexAttribPointer( 1, 2, GL_FLOAT, GL_FALSE, 5 * sizeof( GLfloat ), (GLvoid*)(3 * sizeof( GLfloat )) );

    // Draw element
    glEnableVertexAttribArray( 0 );
    glEnableVertexAttribArray( 1 );
    glDrawArrays( GL_TRIANGLE_FAN, 0, Element.Vertices.Size() );
    glDisableVertexAttribArray( 0 );
    glDisableVertexAttribArray( 1 );

    // Remove element from queue
    CanvasDrawList.Pop();
  }

  // Clean up canvas rendering buffers
  glDeleteBuffers( NumElements, VertexBuffers );

  // Swap buffers to show results of this frame
#if defined LIBUNR_WIN32
  // TODO: Store locally so we don't have to dereference 3 times?
  SwapBuffers( ((UWindowsViewport*)GEngine->Client->CurrentViewport)->DrawContext );
#endif
}

// TODO:
void UOpenGL2RenderDevice::DrawWorld( ULevel* Level, UViewport* Viewport )
{
}

// TODO:
void UOpenGL2RenderDevice::DrawActor( AActor* Actor )
{
}

/*-----------------------------------------------------------------------------
 * Render Device DrawTile
 *
 * Simply initializes a set of FGLVertex instances to describe where this
 * tile will be drawn in the canvas draw step
 *
 * TODO: Make use of vertex indices to use glDrawElements() instead
 -----------------------------------------------------------------------------*/
void UOpenGL2RenderDevice::DrawTile( UTexture* Tex, FBoxInt2D& Dim, FRotator& Rot, float U, float V, float UL, float VL, int PolyFlags )
{
  // Don't draw things that won't show up anyway
  if ( Tex == NULL || (Dim.IsZero()) )
    return;

  FCanvasElement NewElement;

  // Figure out vertex coordinates
  // Here, Min = top left, and Max = bottom right
  FGLVertex Coords[6];

  Coords[0].Vertex.X = Dim.X;
  Coords[0].Vertex.Y = Dim.Y;
  Coords[0].Vertex.Z = 0.0f;
  Coords[0].U = U;
  Coords[0].V = V;

  Coords[1].Vertex.X = Dim.Width;
  Coords[1].Vertex.Y = Dim.Y;
  Coords[1].Vertex.Z = 0.0f;
  Coords[1].U = UL;
  Coords[1].V = V;

  Coords[2].Vertex.X = Dim.X;
  Coords[2].Vertex.Y = Dim.Height;
  Coords[2].Vertex.Z = 0.0f;
  Coords[2].U = U;
  Coords[2].V = VL;

  Coords[3].Vertex.X = Dim.Width;
  Coords[3].Vertex.Y = Dim.Y;
  Coords[3].Vertex.Z = 0.0f;
  Coords[3].U = UL;
  Coords[3].V = V;

  Coords[4].Vertex.X = Dim.X;
  Coords[4].Vertex.Y = Dim.Height;
  Coords[4].Vertex.Z = 0.0f;
  Coords[4].U = U;
  Coords[4].V = VL;

  Coords[5].Vertex.X = Dim.Width;
  Coords[5].Vertex.Y = Dim.Height;
  Coords[5].Vertex.Z = 0.0f;
  Coords[5].U = UL;
  Coords[5].V = VL;

  // Set up canvas element
  NewElement.Vertices.Append( Coords, 6 );
  NewElement.Texture = Tex;
  NewElement.Shader = Shaders[SHADER_DEFAULT];

  // Push to queue
  CanvasDrawList.Push( NewElement );
}

void UOpenGL2RenderDevice::DrawMesh( UMesh* Mesh, FName AnimSeq, float AnimRate, FVector& Loc, FVector& Scale, FRotator& Rot, int PolyFlags )
{
}

// TODO:
void UOpenGL2RenderDevice::DrawGrid( FBox& Dim, FColor& Color )
{
}

/*-----------------------------------------------------------------------------
 * Render Device Viewport Initialization
 *
 * This is largely platform dependent, but has some platform independent code
 *
 * For Windows, we do the following
 * - Make sure the viewport instance type matches our target platform
 * - Get a pixel format that matches what we want for this viewport
 * - Set the pixel format if we have one available
 * - Set our context attributes to target OpenGL 2.1
 * - If we don't have a main rendering context yet, assign it
 * - If we do, make sure all of our resources are shared across contexts
 * - Make our new context the current one
 * - If we're setting up the main context, compile our default shaders
 *
 * After platform specific init, do the following
 * - Set our new GL viewport to match our viewport size
 * - Enable color and depth buffers
 * - Calculate our perspective and orthographic matrices once for later reuse
 * - Push our new viewport into an array
-----------------------------------------------------------------------------*/
bool UOpenGL2RenderDevice::InitViewport( UViewport* Viewport )
{
  FGLViewport GLViewport;

#if defined LIBUNR_WIN32

  // Check if viewport is supported
  if ( Viewport->Class != UWindowsViewport::StaticClass() )
  {
    GLogf( LOG_ERR, "Unknown viewport type '%s', can't create OpenGL context", Viewport->Class->Name.Data() );
    return false;
  }

  UWindowsViewport* WindowsViewport = (UWindowsViewport*)Viewport;
  GLViewport.Viewport = Viewport;
  GLViewport.DC = GetDC( WindowsViewport->Window );

  // TODO: Modify attribs based on config
  const int PixelAttribs[] =
  {
    WGL_DRAW_TO_WINDOW_ARB, GL_TRUE,
    WGL_SUPPORT_OPENGL_ARB, GL_TRUE,
    WGL_DOUBLE_BUFFER_ARB, GL_TRUE,
    WGL_PIXEL_TYPE_ARB, WGL_TYPE_RGBA_ARB,
    WGL_ACCELERATION_ARB, WGL_FULL_ACCELERATION_ARB,
    WGL_COLOR_BITS_ARB, 32,
    WGL_ALPHA_BITS_ARB, 8,
    WGL_DEPTH_BITS_ARB, 24,
    WGL_STENCIL_BITS_ARB, 8,
    0
  };

  int PfdId;
  u32 NumFormats;

  // Get our pixel format
  bool Status = wglChoosePixelFormatARB( GLViewport.DC, PixelAttribs, NULL, 1, &PfdId, &NumFormats );
  if ( !Status || !NumFormats )
  {
    GLogf( LOG_ERR, "No valid pixel format found, can't create OpenGL context" );
    return false;
  }

  PIXELFORMATDESCRIPTOR Pfd;
  DescribePixelFormat( GLViewport.DC, PfdId, sizeof( Pfd ), &Pfd );
  SetPixelFormat( GLViewport.DC, PfdId, &Pfd );

  // Specify an OpenGL 2.1 context
  int ContextAttribs[] =
  {
    WGL_CONTEXT_MAJOR_VERSION_ARB, 2,
    WGL_CONTEXT_MINOR_VERSION_ARB, 1,
    WGL_CONTEXT_PROFILE_MASK_ARB, WGL_CONTEXT_CORE_PROFILE_BIT_ARB,
    0
  };

  // Check if this context type is supported
  GLViewport.RC = wglCreateContextAttribsARB( GLViewport.DC, MainRC, ContextAttribs );
  if ( !GLViewport.RC )
  {
    GLogf( LOG_ERR, "OpenGL 2.1 context is not supported" );
    return false;
  }

  // All contexts should share the same resources
  if ( !MainRC )
  {
    MainRC = GLViewport.RC;
  }
  else
  {
    if ( !wglShareLists( GLViewport.RC, MainRC ) )
    {
      GLogf( LOG_ERR, "Failed to share display lists from main context to sub context" );
      return false;
    }
  }


  // Set the new context to current
  if ( !wglMakeCurrent( GLViewport.DC, GLViewport.RC ) )
  {
    GLogf( LOG_ERR, "Couldn't bind new OpenGL 2.1 context" );
    return false;
  }

  // Compile shaders for our main RC
  if ( MainRC == GLViewport.RC )
  {
    if ( !CompileShaderPrograms() )
    {
      GLogf( LOG_ERR, "Failed to compile default shaders" );
      return false;
    }
  }

#else
  GLogf( LOG_ERR, "Unsupported viewport type '%s", Viewport->Class->Name.Data() );
  return false;
#endif

  // Init viewport
  glViewport( 0, 0, Viewport->Width, Viewport->Height );

  // Set global state properties
  glClearColor( 0.0f, 0.0f, 0.0f, 0.0f );
  glEnable( GL_DEPTH_TEST );
  glEnable( GL_BLEND );
  glBlendFunc( GL_SRC_ALPHA, GL_SRC_COLOR );

  // Setup viewport projection matrices
  GetPerspectiveMatrix( GLViewport.Perspective, 0.0f, Viewport->Width, 0.0f, Viewport->Height, 1.0f, 1024.0f );
  GetOrthoMatrix( GLViewport.Perspective, 0.0f, Viewport->Width, 0.0f, Viewport->Height, -1.0f, 1.0f );

  // Keep track of our new viewport
  Viewports.PushBack( GLViewport );
  CurrentViewport = &Viewports[Viewports.Size()-1];
  return true;
}

bool UOpenGL2RenderDevice::SetActiveViewport( UViewport* Viewport )
{
  return false;
}

/*-----------------------------------------------------------------------------
 * Render Device Texture Registration
 *
 * The general order of operations is as follows
 * - Create a new texture handle
 * - Bind the new texture handle
 * - Set texture filtering based on texture properties
 * - Set texture wrap setting based on texture properties
 * - Prepare the texture for upload to the graphics device
 * - Send the texture to the graphics device
 * - Free any leftover memory from texture preparation
 * - Set the texture's handle to our newly made GL handle
 *
 * For standard "P8" textures, do the following
 * - Create a buffer that fits each channel for each pixel
 * - Iterate through each pixel
 * - Grab the palette index for that pixel
 * - Unpack each color channel into our created buffer
 * - Buffer is ready for device upload
 *
 * TODO: Check render options for disabling all texture filtering
-----------------------------------------------------------------------------*/
bool UOpenGL2RenderDevice::RegisterTexture( UTexture* Texture )
{
  // Create texture handle
  GLuint TextureHandle;
  glGenTextures( 1, &TextureHandle );
  glBindTexture( GL_TEXTURE_2D, TextureHandle );
  
  // Set texture filtering
  glTexParameteri( GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, (Texture->bNoSmooth) ? GL_NEAREST : GL_LINEAR );
  glTexParameteri( GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, (Texture->bNoSmooth) ? GL_NEAREST : GL_LINEAR );

  // Set texture wrapping
  GLenum UClampMode;
  GLenum VClampMode;

  switch ( Texture->UClampMode )
  {
    case UWrap:
      UClampMode = GL_REPEAT;
      break;
    case UClamp:
      UClampMode = GL_CLAMP_TO_EDGE;
      break;
  }

  switch ( Texture->VClampMode )
  {
  case VWrap:
    VClampMode = GL_REPEAT;
    break;
  case UClamp:
    VClampMode = GL_CLAMP_TO_EDGE;
    break;
  }

  glTexParameteri( GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, UClampMode );
  glTexParameteri( GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, VClampMode );

  // TODO: Any other parameters that need to be set?

  
  // Unpack the texture to a buffer
  u8* TexBuf = Texture->Mips[0].DataArray.Data();
  u8* UnpackBuf = new u8[Texture->USize * Texture->VSize * 3];
  u8* UnpackPtr = UnpackBuf;

  FColor* Palette = Texture->Palette->Colors;

  for ( int i = Texture->VSize - 1; i >= 0; i-- )
  {
    for ( int j = 0; j < Texture->USize; j++ )
    {
      u8 PalIdx = TexBuf[(i * Texture->USize) + j];

      *UnpackPtr++ = Palette[PalIdx].R;
      *UnpackPtr++ = Palette[PalIdx].G;
      *UnpackPtr++ = Palette[PalIdx].B;
    }
  }

  // TODO: Mipmapping
  // Buffer texture data
  glTexImage2D( GL_TEXTURE_2D, 0, GL_RGBA, Texture->USize, Texture->VSize, 0, GL_RGB, GL_UNSIGNED_BYTE, UnpackBuf );

  // Free temporary buffer
  delete[] UnpackBuf;

  // Set texture's texture handle
  Texture->TextureHandle = (void*)TextureHandle;

  return true;
}

/*-----------------------------------------------------------------------------
 * Render Device Texture Removal
 *
 * Simply check if the texture has a valid handle, and delete it if so
-----------------------------------------------------------------------------*/
bool UOpenGL2RenderDevice::UnregisterTexture( UTexture* Texture )
{
  if ( Texture->TextureHandle )
  {
    GLuint TextureHandle = (GLuint)Texture->TextureHandle;
    glDeleteTextures( 1, &TextureHandle );
  }

  return true;
}

/*-----------------------------------------------------------------------------
 * Render Device Shader Compilation
 *
 * Compile our set of frequently used shaders for normal rendering
 * This list includes the following
 * - A default shader (CURRENTLY IN TESTING)
-----------------------------------------------------------------------------*/
bool UOpenGL2RenderDevice::CompileShaderPrograms()
{
  FGLShaderProgram Shader;

  // Go to libunr root directory
  char* Tmp = getcwd( NULL, 0 );
  chdir( GSystem->LibunrPath );

  FStringFilePath VertPath( "OpenGL2", "DefaultShader", "vert" );
  FStringFilePath FragPath( "OpenGL2", "DefaultShader", "frag" );

  // Compile default shader
  if ( !Shader.VertexShader.LoadAndCompile( VertPath ) )
    return false;
  if ( !Shader.FragmentShader.LoadAndCompile( FragPath ) )
    return false;
  if ( !Shader.Link() )
    return false;

  Shaders.PushBack( Shader );

  // Restore last directory and cleanup
  chdir( Tmp );
  free( Tmp );

  return true;
}

IMPLEMENT_MODULE_CLASS( UOpenGL2RenderDevice );
