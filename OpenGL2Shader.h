/*===========================================================================*\
|*  libunr-OpenGL2Drv - An OpenGL2 Render Device for libunr                  *|
|*  Copyright (C) 2018-2019  Adam W.E. Smith                                 *|
|*                                                                           *|
|*  This program is free software: you can redistribute it and/or modify     *|
|*  it under the terms of the GNU General Public License as published by     *|
|*  the Free Software Foundation, either version 3 of the License, or        *|
|*  (at your option) any later version.                                      *|
|*                                                                           *|
|*  This program is distributed in the hope that it will be useful,          *|
|*  but WITHOUT ANY WARRANTY; without even the implied warranty of           *|
|*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the            *|
|*  GNU General Public License for more details.                             *|
|*                                                                           *|
|*  You should have received a copy of the GNU General Public License        *|
|*  along with this program. If not, see <https://www.gnu.org/licenses/>.    *|
\*===========================================================================*/

/*========================================================================
 * OpenGL2Shader.h - OpenGL 2.x shader object
 *
 * written by Adam 'Xaleros' Smith
 *========================================================================
*/

#pragma once

#include <Util/FFileArchive.h>

#define SHADER_DEFAULT 0

/*-----------------------------------------------------------------------------
 * FGLShader
 * A single GLSL shader
-----------------------------------------------------------------------------*/
class FGLShader
{
public:
  FGLShader();
  ~FGLShader();

  bool LoadAndCompile( FStringFilePath& Filepath );

  const char* Name;
  u32 Handle;
  bool bCompiled;
};

/*-----------------------------------------------------------------------------
 * FGLShaderProgram
 * A single GLSL program linked with one shader of each type
-----------------------------------------------------------------------------*/
class FGLShaderProgram
{
public:
  FGLShaderProgram();
  ~FGLShaderProgram();

  bool Link();
  void Use();

  u32 Handle;
  bool bLinked;

  FGLShader VertexShader;
  FGLShader FragmentShader;
  FGLShader GeometryShader;
};
