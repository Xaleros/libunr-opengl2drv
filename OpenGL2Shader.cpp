#include "OpenGL2Shader.h"
/*===========================================================================*\
|*  libunr-OpenGL2Drv - An OpenGL2 Render Device for libunr                  *|
|*  Copyright (C) 2018-2019  Adam W.E. Smith                                 *|
|*                                                                           *|
|*  This program is free software: you can redistribute it and/or modify     *|
|*  it under the terms of the GNU General Public License as published by     *|
|*  the Free Software Foundation, either version 3 of the License, or        *|
|*  (at your option) any later version.                                      *|
|*                                                                           *|
|*  This program is distributed in the hope that it will be useful,          *|
|*  but WITHOUT ANY WARRANTY; without even the implied warranty of           *|
|*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the            *|
|*  GNU General Public License for more details.                             *|
|*                                                                           *|
|*  You should have received a copy of the GNU General Public License        *|
|*  along with this program. If not, see <https://www.gnu.org/licenses/>.    *|
\*===========================================================================*/

/*========================================================================
 * OpenGL2Shader.h - OpenGL 2.x shader object
 *
 * written by Adam 'Xaleros' Smith
 *========================================================================
*/

#include "OpenGL2Shader.h"
#include "OpenGL2Funcs.h"

#include <Util/FLogFile.h>

/*-----------------------------------------------------------------------------
 * FGLShader
-----------------------------------------------------------------------------*/
FGLShader::FGLShader()
{
  Name = NULL;
  Handle = 0;
  bCompiled = false;
}

FGLShader::~FGLShader()
{
}

bool FGLShader::LoadAndCompile( FStringFilePath& Filepath )
{
  FFileArchiveIn In;
  GLenum ShaderType;

  // Figure out the shader type
  Name = Filepath.GetName();
  const char* Ext = Filepath.GetExt();
  if ( Ext == NULL )
  {
  UnknownShaderType:
    GLogf( LOG_ERR, "Unknown shader type for file '%s", Name );
    return false;
  }

  if ( strcmp( Ext, "vert" ) == 0 )
    ShaderType = GL_VERTEX_SHADER;
  else if ( strcmp( Ext, "frag" ) == 0 )
    ShaderType = GL_FRAGMENT_SHADER;
  else if ( strcmp( Ext, "geom" ) == 0 )
    ShaderType = GL_GEOMETRY_SHADER;
  else
    goto UnknownShaderType;

  Handle = glCreateShader( ShaderType );
  Name = strdup( Filepath.GetName() );

  // Open shader code
  if ( In.Open( Filepath.Data() ) < 0 )
  {
    GLogf( LOG_ERR, "Can't open shader file '%s'", Filepath.Data() );
    glDeleteShader( Handle );
    Handle = 0;
    return false;
  }

  // Read in the whole file
  In.Seek( 0, End );
  int Len = In.Tell();
  In.Seek( 0, Begin );

  char* Code = new char[Len];
  In.Read( Code, Len );

  // Attempt to compile shader
  glShaderSource( Handle, 1, &Code, &Len );
  glCompileShader( Handle );

  // Check for failure
  int Success;
  char Log[512];
  glGetShaderiv( Handle, GL_COMPILE_STATUS, &Success );
  if ( !Success )
  {
    glGetShaderInfoLog( Handle, 512, NULL, Log );
    GLogf( LOG_ERR, "Failed to compile shader '%s'", Name );
    GLogf( LOG_ERR, Log );
    delete[] Code;
    glDeleteShader( Handle );
    Handle = 0;
    return false;
  }

  bCompiled = true;
  return true;
}

/*-----------------------------------------------------------------------------
 * FGLShaderProgram
-----------------------------------------------------------------------------*/
FGLShaderProgram::FGLShaderProgram()
{
  Handle = 0;
  bLinked = false;
}

FGLShaderProgram::~FGLShaderProgram()
{
}

bool FGLShaderProgram::Link()
{
  int Success = 0;
  char Log[512];
  
  Handle = glCreateProgram();

  // Attach vertex shader
  if ( VertexShader.bCompiled )
  {
    glAttachShader( Handle, VertexShader.Handle );
  }
  else
  {
    GLogf( LOG_ERR, "No vertex shader specified in program" );
    return false;
  }

  // Attach fragment shader
  if ( FragmentShader.bCompiled )
  {
    glAttachShader( Handle, FragmentShader.Handle );
  }
  else
  {
    GLogf( LOG_ERR, "No fragment shader specified in program" );
    return false;
  }

  // Attach optional geometry shader
  if ( GeometryShader.bCompiled )
    glAttachShader( Handle, GeometryShader.Handle );

  glLinkProgram( Handle );
  glGetProgramiv( Handle, GL_LINK_STATUS, &Success );
  if ( !Success )
  {
    glGetProgramInfoLog( Handle, 512, NULL, Log );
    GLogf( LOG_ERR, "Failed to link shader program '%s'", VertexShader.Name );
    GLogf( LOG_ERR, Log );
    glDeleteProgram( Handle );
    Handle = 0;
    return false;
  }

  bLinked = true;
  return true;
}

void FGLShaderProgram::Use()
{
  glUseProgram( Handle );
}
